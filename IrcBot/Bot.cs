﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// these classes handle stremaing, network connection, and threading apparently
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace IrcBot
{
    enum MessageType {
        NormalMessage,
        ActionMessage,
        PrivateMessage
    }

    class Bot
    {

        // Irc server to connect 
        public static string SERVER = "irc.freenode.net";

        // Irc server's port (6667 is default port)
        private static int PORT = 6667;

        // User information defined in RFC 2812 (Internet Relay Chat: Client Protocol) is sent to irc server 
        private static string USER = "Zekerai";

        // Bot's nickname
        private static string NICK = "Zekerai";

        // Channel to join
        private static string CHANNEL = "##facerig";

        // StreamWriter is declared here so that PingSender can access it
        public static StreamWriter writer;

        private static TcpClient irc;
        private static NetworkStream stream;
        private static StreamReader reader;
        private static bool JOINED_CANNEL;
        private static string InputLine;

        private static List<string> channelUsers;


        public static void Main(string[] args)
        {
            //User newUser = new User(SERVER, PORT, USER, NICK, CHANNEL);
            //newUser.ConnectToServer();
            //newUser.SetUpUser();
            //newUser.ListenForStreamData();

            JOINED_CANNEL = false;
            try
            {
                while (irc == null)
                {
                    irc = new TcpClient(SERVER, PORT);
                    stream = irc.GetStream();
                    reader = new StreamReader(stream);
                    writer = new StreamWriter(stream);

                }



                ListenAndWatchForIncomingData();

            }
            catch (Exception e)
            {
                // Show the exception, sleep for a while and try to establish a new connection to irc server
                Console.WriteLine(e.ToString());
                Thread.Sleep(5000);

            }
        }

       
        


        static void ListenAndWatchForIncomingData()
        {
            while (true)
            {
                while ((InputLine = reader.ReadLine()) != null)
                {
                    Console.WriteLine(InputLine);
                    String[] serverBrokenUp = SERVER.Split(".".ToCharArray());
                    String domainName = String.Format("{0}.{1}", serverBrokenUp[serverBrokenUp.Length - 2], serverBrokenUp[serverBrokenUp.Length - 1]);

                    String stringReceivedIndicatingWeCanStartJoiningChannels = String.Format("{0} 376 {1}", domainName, NICK);

                    if (InputLine.Contains(stringReceivedIndicatingWeCanStartJoiningChannels))
                    {
                        JoinChannel();
                    }

                    if (InputLine.StartsWith("PING :"))
                    {
                        SendPong();
                    }

                    if (InputLine.EndsWith("JOIN :" + CHANNEL))
                    {
                        GreetNewlyJoinedNick();
                        RetrieveAllUsers();
                    }

                    if (InputLine.EndsWith("PRIVMSG " + CHANNEL + " :" + NICK + " leave")) 
                    {
                        Quit();
                    }

                    if (InputLine.Contains("KICK " + CHANNEL + " " + NICK))
                    {
                        JOINED_CANNEL = false;
                        JoinChannel();
                    }

                    if (InputLine.Contains( NICK + " = " + CHANNEL + " :" ) )
                    {
                        channelUsers = new List<string>();

                        string searchedString = NICK + " = " + CHANNEL + " :";
                        int indexOfString = InputLine.IndexOf(searchedString);
                        string usersOfChannel = InputLine.Substring(searchedString.Length + indexOfString);


                        string[] newArrayOfChannelUsers = usersOfChannel.Split(' ');

                        foreach (string user in newArrayOfChannelUsers)
                        {

                            if (String.IsNullOrEmpty(user) || user.Equals(NICK))
                                continue;
                            char subString = user[0];
                            if (Char.IsLetter(subString))
                            {
                                channelUsers.Add(user);
                            }
                            else
                            {
                                channelUsers.Add(user.Substring(1));
                            }
                        }

                    }

                    if (InputLine.Contains("PRIVMSG " + CHANNEL + " :" + NICK + " hug "))
                    {

                        string[] inputLineArray = InputLine.Split(' ');
                        string currentUser = inputLineArray[5];


                        bool valid = ValidName(currentUser);
                        if (valid)
                        {
                            Speak("hugs " + currentUser, CHANNEL, MessageType.ActionMessage);
                        }
                        else
                        {
                            Speak("would hug that person but he can't find them......", CHANNEL, MessageType.ActionMessage);
                        }

                    }

                    if (InputLine.Contains("PRIVMSG " + CHANNEL + " :" + "!whatis "))
                    {
                        string[] inputLineArray = InputLine.Split(' ');

                        string currentUser = inputLineArray[4];
                        string message = retrieveUserDesciption(currentUser);

                        Console.WriteLine("Current User: " + currentUser);
                        Console.WriteLine("Message: " + message);

                        Speak(message, CHANNEL, MessageType.NormalMessage);
                    }

                    if (InputLine.Contains("PRIVMSG " + CHANNEL + " :" + "!iam "))
                    {
                        string[] inputLineArray = InputLine.Split('!');
                        string colonName = inputLineArray[0];

                        Console.WriteLine("ColonName: " + colonName);
                        string testingString = "PRIVMSG " + CHANNEL + " :" + "!iam ";

                        string currentUser = "";

                        for (int i = 0; i < colonName.Length; i++)
                        {
                            char nextChar = colonName[i];
                            if (!Char.IsLetter(nextChar))
                            {
                                currentUser = colonName.Substring(i + 1);
                                Console.WriteLine("Cut User: " + currentUser);
                            }
                        }

                        string desciption = InputLine.Substring(InputLine.IndexOf("!iam ") + 4);

                        Console.WriteLine("User: " + currentUser);
                        Console.WriteLine("Description: " + desciption);

                        storeUserDesciption(desciption, currentUser);

                        // Speak("nothing ", CHANNEL, MessageType.NormalMessage);
                    }

                }
                // Close all streams
                writer.Close();
                reader.Close();
                irc.Close();
            }
        }

        static void SendPong()
        {
            string pongRequest = InputLine.Substring(6, InputLine.Count() - 6);
            pongRequest = string.Format("PONG :{0}", pongRequest);

            writer.WriteLine(pongRequest);
            Console.WriteLine(pongRequest);
            writer.Flush();
        }

        static void GreetNewlyJoinedNick()
        {
            // Parse nickname of person who joined the channel
            string nickname = InputLine.Substring(1, InputLine.IndexOf("!") - 1);

            // Welcome the nickname to channel by sending a notice
            writer.WriteLine("NOTICE " + nickname + " :Hi " + nickname +
            " and welcome to " + CHANNEL + " channel!");

            Console.WriteLine("NOTICE " + nickname + " :Hi " + nickname +
            " and welcome to " + CHANNEL + " channel!");

            writer.Flush();
            // Sleep to prevent excess flood
            Thread.Sleep(2000);
        }

        static void JoinChannel()
        {
            if (JOINED_CANNEL == false)
            {
                writer.WriteLine("JOIN " + CHANNEL);
                writer.Flush();
                Console.WriteLine("JOIN " + CHANNEL);
                JOINED_CANNEL = true;

            }
        }

        static void Speak(string message, string target, MessageType type)
        {
            if (type == MessageType.NormalMessage)
            {
                writer.WriteLine("PRIVMSG " + target + " " + message);
                Console.WriteLine("PRIVMSG " + target + " " + message);
                writer.Flush();
            }
            else if (type == MessageType.ActionMessage)
            {
                writer.WriteLine("PRIVMSG " + target + " :\x0001ACTION " + message + "\x0001");
                Console.WriteLine("PRIVMSG " + target + " :\x0001ACTION " + message + "\x0001");
                writer.Flush();
            }
            else if (type == MessageType.PrivateMessage)
            {
                writer.WriteLine("PRIVMSG " + target + " " + message);
                Console.WriteLine("PRIVMSG " + target + " " + message);
                writer.Flush();
            }
        }

        static void Quit()
        {
            Environment.Exit(0);
        }

        static void RetrieveAllUsers()
        {
             writer.WriteLine("NAMES " + CHANNEL);
             writer.Flush();
             Console.WriteLine("NAMES " + CHANNEL);
        }

        static bool ValidName(string name)
        {
            foreach (string user in channelUsers)
            {
                Console.WriteLine(user);

                if (String.IsNullOrEmpty(user))
                    continue;

                if (name.Equals(user))
                {
                    return true;
                }
            }


            return false;
        }

        static string retrieveUserDesciption(string user)
        {
            string path = Path.Combine(Environment.CurrentDirectory, "userDescription.txt");
            string[] lines = System.IO.File.ReadAllLines(path);

            foreach (string description in lines)
            {
                if (description.StartsWith(user))
                {
                    return description;
                }
            }

            return "I can't find a record of them, did they ever set an !iam?";
        }

        static void storeUserDesciption(string description, string user)
        {

            string path = Path.Combine(Environment.CurrentDirectory, "userDescription.txt");
            string[] lines = System.IO.File.ReadAllLines(path);

            List<string> linesList = lines.OfType<string>().ToList<string>();

            foreach (string entry in linesList)
            {
                if (entry.StartsWith(user))
                {
                    linesList.Remove(entry);
                    break;
                }
            }

            string message = user + " is" + description;
            linesList.Add(message);

            System.IO.File.WriteAllLines(path, linesList);

            Speak(message, CHANNEL, MessageType.NormalMessage);
        }

    }
}
