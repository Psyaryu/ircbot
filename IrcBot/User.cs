﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// these classes handle stremaing, network connection, and threading apparently
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading; 

namespace IrcBot
{
    class User
    {

        public string SERVER { get; set;}
        public int PORT { get; set;}
        public string USER { get; set;}
        public string NICK { get; set; }
        public string CHANNEL { get; set; }
        public bool needToJoinChannel { get; set; }
        public bool connectedToIRC { get; set; }

        public TcpClient tcpClient { get; set; }
        public NetworkStream networkStream { get; set; }
        public StreamReader readerStream { get; set; }
        public StreamWriter writerStream { get; set; }

        public string receivedDataStream { get; set; }

        public string stringReceivedIndicatingWeCanStartJoiningChannels { get; set; }
        public string pingCommand { get; set; }

        public User( string server, int port, string user, string nick, string channel  )
        {
            this.SERVER = server;
            this.PORT = port;
            this.NICK = nick;
            this.CHANNEL = channel;
            this.USER = user;

            // due to the server having a wildcard domain
            // we must only account for the last part
            // aka out of dickens.freenode.net this will grab
            // freenode.net
            String[] serverBrokenUp = server.Split(".".ToCharArray());
            String domainName = String.Format("{0}.{1}", serverBrokenUp[serverBrokenUp.Length - 2], serverBrokenUp[serverBrokenUp.Length - 1]);

            this.stringReceivedIndicatingWeCanStartJoiningChannels = String.Format("{0} 376 {1}", domainName, nick);
            this.pingCommand = "PING :";
        }

        public void ConnectToServer()
        {
            needToJoinChannel = false;
            try
            {
                while (this.tcpClient == null)
                {
                    this.tcpClient = new TcpClient(this.SERVER, this.PORT);
                    this.networkStream = this.tcpClient.GetStream();
                    this.readerStream = new StreamReader(this.networkStream);
                    this.writerStream = new StreamWriter(this.networkStream);

                    this.connectedToIRC = true;
                }


            }
            catch (Exception e)
            {
                // Show the exception, sleep for a while and try to establish a new connection to irc server
                Console.WriteLine(e.ToString());
                Thread.Sleep(5000);

            }
        }

        public void SetUpUser()
        {
            // set up the connection via raw protocol to an irc
            // establish a connection to the server
            this.writerStream.WriteLine("USER " + USER + " 8 * :" + USER);
            this.writerStream.Flush();
            Console.WriteLine("USER " + USER + " 8 * :" + USER);

            this.writerStream.WriteLine("NICK " + NICK);
            this.writerStream.Flush();
            Console.WriteLine("NICK " + NICK);
        }

        public void JoinChannel()
        {
            if (this.needToJoinChannel == false)
            {
                this.writerStream.WriteLine("JOIN " + this.CHANNEL);
                this.writerStream.Flush();
                Console.WriteLine("JOIN " + this.CHANNEL);
                this.needToJoinChannel = true;

            }
        }

        public void PongServer()
        {
            string pongRequest = this.receivedDataStream.Substring(this.pingCommand.Length, this.receivedDataStream.Count() - this.pingCommand.Length);
            pongRequest = string.Format("PONG :{0}", pongRequest);

            this.writerStream.WriteLine(pongRequest);
            Console.WriteLine(pongRequest);
            this.writerStream.Flush();
        }

        public string stripCommandOfForeignCharacters(string command)
        {
            foreach (char character in command)
            {
                if ( Char.IsLetter(character) || character.Equals('!')) 
                {
                    int index = command.IndexOf(character);
                    return command.Substring(index);
                }
            }

            return "";
        }

        public bool isValidCommand(string enteredCommand) 
        {
            string path = Path.Combine(Environment.CurrentDirectory, "commandList.txt");
            string[] allCommands = System.IO.File.ReadAllLines(path);

            foreach (string command in allCommands)
            {
                if (enteredCommand.Equals(command))
                    return true;
            }

            return false;
        }

        public void BuildCommands()
        {
            string path = Path.Combine(Environment.CurrentDirectory, "commandList.txt");
            List<string> commandList = new List<string>();

            commandList.Add("!iam");
            commandList.Add("!whatis");
            commandList.Add(String.Format("{0} hug", NICK));
            commandList.Add(String.Format("{0} bap", NICK));

            System.IO.File.WriteAllLines(path,  commandList);

        }

        public void ListenForStreamData()
        {
            while (this.connectedToIRC)
            {
                this.receivedDataStream = this.readerStream.ReadLine();
                while (this.receivedDataStream != null)
                {
                    Console.WriteLine(this.receivedDataStream);

                    // we need to check if the input string contains PRIVMSG telling us that someone said something
                    if (this.receivedDataStream.Contains("PRIVMSG"))
                    {
                        string[] receivedDataStreamSplitBySpace = this.receivedDataStream.Split(' ');

                        string command = receivedDataStreamSplitBySpace[3];

                        // we need to test if this is a single phrase command or we are directly interacting with the bot
                        // for AI user experience, this could be multiple phrases
                        if (command.Contains(this.NICK))
                            command = String.Format("{0} {1}", receivedDataStreamSplitBySpace[3], receivedDataStreamSplitBySpace[4]);

                        command = this.stripCommandOfForeignCharacters(command);

                        if (this.isValidCommand(command))
                        {


                        }
                    }
                    if (this.receivedDataStream.StartsWith(this.pingCommand))
                        this.PongServer();

                    if (this.receivedDataStream.Contains(this.stringReceivedIndicatingWeCanStartJoiningChannels))
                        this.JoinChannel();

                    // reset the stored data stream
                    this.receivedDataStream = null;
                }
            }

            // no longer connected to the server
            // Close all streams
            this.writerStream.Close();
            this.readerStream.Close();
            this.tcpClient.Close();
        }

    }
}
